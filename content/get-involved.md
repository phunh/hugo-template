---
layout: get-involved
title: Get Involved
name: Elisa
userbase: Elisa
menu:
  main:
    weight: 4
getintouch: |
  Most development-related discussions take place on the [Elisa mailing list](http://mail.kde.org/mailman/listinfo/elisa).
  Just join in, say hi and tell us what you would like to help us with!
---

Want to contribute to Elisa? Check out [Bugzilla](https://bugs.kde.org) for some fun tasks or [browse the source code](https://invent.kde.org/).
